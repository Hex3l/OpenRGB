/*-----------------------------------------*\
|  MSIGPUControllerDetect.cpp               |
|                                           |
|  Driver for MSI GPUs                      |
|                                           |
\*-----------------------------------------*/

#include "MSIGPUController.h"
#include "RGBController.h"
#include "RGBController_MSIGPU.h"
#include "i2c_smbus.h"
#include <vector>
#include <stdio.h>
#include <stdlib.h>

/******************************************************************************************\
*                                                                                          *
*   IsMSIGPUController                                                                     *
*                                                                                          *
*       Compare PCI IDs                                                                    *
*                                                                                          *
\******************************************************************************************/

bool IsMSIGPUController(i2c_smbus_interface* bus)
{
    const unsigned char dev = 0x68;

    bool pass = false;

    if (bus->port_id != 1) {
        return(pass);
    }

    // Checks for an MSI vga using NVidia platform
    if (bus->pci_vendor != 0x10de
            && bus->pci_subsystem_vendor != 0x1462) {
        return(pass);
    }

    //Writes to gpu first RGB register
    int res_r1 =  bus->i2c_smbus_write_byte_data(dev, MSI_GPU_REG_R1, 0xFF);
    int res_g1 = bus->i2c_smbus_write_byte_data(dev, MSI_GPU_REG_G1, 0xFF);
    int res_b1 = bus->i2c_smbus_write_byte_data(dev, MSI_GPU_REG_B1, 0xFF);

    //Checks if the values are written without errors
    if(res_r1 >= 0
            && res_g1 >= 0
            && res_b1 >= 0)
    {
        pass = true;
    }

    return(pass);
}   /* IsMSIGPUController() */


/******************************************************************************************\
*                                                                                          *
*   DetectMSIGPUControllers                                                               *
*                                                                                          *
*       Detect MSI GPU controllers on the enumerated I2C busses.                          *
*                                                                                          *
\******************************************************************************************/

void DetectMSIGPUControllers(std::vector<i2c_smbus_interface*> &busses, std::vector<RGBController*> &rgb_controllers)
{
    MSIGPUController* new_msi_gpu;
    RGBController_MSIGPU* new_controller;

    for (unsigned int bus = 0; bus < busses.size(); bus++)
    {
        if (IsMSIGPUController(busses[bus]))
        {
            new_msi_gpu = new MSIGPUController(busses[bus]);
            new_controller = new RGBController_MSIGPU(new_msi_gpu);
            rgb_controllers.push_back(new_controller);
        }
    }
} /* DetectMSIGPUControllers() */
